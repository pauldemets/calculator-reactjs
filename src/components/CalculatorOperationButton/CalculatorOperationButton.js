/* eslint-disable react/prop-types */
import React from 'react';
import './scss/CalculatorOperationButton.scss';

function CalculatorOperationButton({ libelle = null, handleClick }) {
	return (
		<button className="calculator-operation-button" onClick={handleClick}>
			{libelle ?? 'default'}
		</button>
	);
}

export default CalculatorOperationButton;
