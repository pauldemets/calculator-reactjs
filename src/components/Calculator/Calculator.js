/* eslint-disable indent */
import React, { useEffect, useState } from 'react';
import './scss/Calculator.scss';
import CalculatorNumberButton from '../CalculatorNumberButton/CalculatorNumberButton';
import CalculatorOperationButton from '../CalculatorOperationButton/CalculatorOperationButton';
import Numbers from './consts/Numbers';
import Operations from './consts/Operations';
import OtherSymbols from './consts/OtherSymbols';

function Calculator() {
	const [calculatorNumber1, setCalculatorNumber1] = useState('');
	const [calculatorNumber2, setCalculatorNumber2] = useState('');
	const [currentOperator, setCurrentOperator] = useState(null);
	const [calculHistory, setCalculHistory] = useState(null);
	const [result, setResult] = useState('');

	const renderNumbers = Numbers.map((number, i) => {
		return (
			<div className='number-wrapper column' key={i}>
				<CalculatorNumberButton
					libelle={number}
					handleClick={() => handlePressNumber(number)}
				/>
			</div>
		);
	});

	const renderOperations = Operations.map((operation, index) => {
		return (
			<div className='operation-wrapper column' key={index}>
				<CalculatorOperationButton
					libelle={operation}
					key={index}
					handleClick={() => handleOperationAction(operation)}
				/>
			</div>
		);
	});

	const renderOtherSymbols = OtherSymbols.map((symbol, index) => {
		return (
			<div className='operation-wrapper column' key={index}>
				<CalculatorOperationButton
					libelle={symbol}
					key={index}
					handleClick={() => handleOperationAction(symbol)}
				/>
			</div>
		);
	});

	useEffect(() => {
		setCalculHistory(`
			${calculatorNumber1 ?? ''} 
			${currentOperator ?? ''} 
			${calculatorNumber2 ?? ''}
		`);
	}, [calculatorNumber1, calculatorNumber2, currentOperator]);


	const handlePressCalculatorOperator = (operator) => {
		if (currentOperator) {
			setCalculatorNumber1(result);
			setCalculatorNumber2('');
		}
		setCurrentOperator(operator);
	};

	const handlePressNumber = (number) => {
		if (currentOperator) {
			setCalculatorNumber2(calculatorNumber2 + number);
		}
		else {
			setCalculatorNumber1(calculatorNumber1 + number);
		}
	};


	const handleEraseNumber = () => {
		if (currentOperator) {
			setCalculatorNumber2(calculatorNumber2.slice(0, -1));
		}
		else {
			setCalculatorNumber1(calculatorNumber1.slice(0, -1));
		}
	};

	const handleClear = () => {
		setCalculatorNumber1('');
		setCalculatorNumber2('');
		setCurrentOperator(null);
		setCalculHistory('');
		setResult('');
	};

	const handleOperationAction = (operator) => {
		switch (true) {
			case operator === '=':
				handleCalculateResult();
				break;
			case operator === 'CE':
				handleClear();
				break;
			case operator === '←':
				handleEraseNumber();
				break;
			case operator === ',':
				handleAddDecimalPoint();
				break;
			case operator === '±':
				handleOpposite();
				break;
			default:
				handlePressCalculatorOperator(operator);
				break;
		}
	};

	const handleOpposite = () => {
		if (calculatorNumber2) {
			setCalculatorNumber2(-calculatorNumber2);
		}
		else if (calculatorNumber1) {
			setCalculatorNumber1(-calculatorNumber1);
		}
	};

	const handleAddDecimalPoint = () => {
		if (currentOperator) {
			setCalculatorNumber1(calculatorNumber2 + '.');
		}
		else {
			setCalculatorNumber1(calculatorNumber1 + '.');
		}
	};

	const handleCalculateResult = () => {
		switch (true) {
			case currentOperator === '*':
				setResult(
					parseFloat(calculatorNumber1) * parseFloat(calculatorNumber2)
				);
				break;
			case currentOperator === '+':
				setResult(
					parseFloat(calculatorNumber1) + parseFloat(calculatorNumber2)
				);
				break;
			case currentOperator === '-':
				setResult(
					parseFloat(calculatorNumber1) - parseFloat(calculatorNumber2)
				);
				break;
			case currentOperator === '/':
				setResult(
					parseFloat(calculatorNumber1) / parseFloat(calculatorNumber2)
				);
				break;
			case currentOperator === '÷':
				setResult(
					parseFloat(calculatorNumber1) % parseFloat(calculatorNumber2)
				);
				break;
			default:
				break;
		}
	};


	return (
		<div id='calculator' className='column'>
			<div id='calculator-wrapper' className='column'>
				<div className="column" id="calcul-history-container">
					<span>
						{calculHistory}&nbsp;
					</span>
				</div>
				<input type='number' disabled id='calculator-input' defaultValue={result} />
				<div className="row" id="other-symbols-container">
					{renderOtherSymbols}
				</div>
				<div className='row'>
					<div id='numbers-container' className='row'>
						{renderNumbers}
					</div>
					<div id='operations-container' className='column'>
						{renderOperations}
					</div>
				</div>
			</div>
		</div>
	);
}

export default Calculator;
