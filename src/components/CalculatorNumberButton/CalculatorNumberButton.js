/* eslint-disable react/prop-types */
import React from 'react';
import './scss/CalculatorNumberButton.scss';

function CalculatorNumberButton({ libelle = null, handleClick }) {
	return (
		<button className="calculator-button" onClick={handleClick}>
			{libelle ?? 'default'}
		</button>
	);
}

export default CalculatorNumberButton;
